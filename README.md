# Project highlights
## Embedded

### <img src="https://gitlab.com/uploads/-/system/project/avatar/57643378/repopic.png" width="24"/> [synth0](https://gitlab.com/slusheea/synth0)
Experimental hardware synthesiser based on the RP2040

### <img src="https://gitlab.com/uploads/-/system/project/avatar/40522316/repopic.png" width="24"/> [Plant monitor](https://gitlab.com/slusheea/plant-monitor)
Monitor a plant's envoriment remotely through Grafana with a microcontroller

### <img src="https://gitlab.com/uploads/-/system/project/avatar/38977200/repopicture_png.png" width="24"/> [µtop](https://gitlab.com/slusheea/utop)
Display your computer's core stats with an embedded LCD

## Crates
### <img src="https://gitlab.com/uploads/-/system/project/avatar/55514341/repopic.png" width="24"/> [stk8ba58](https://gitlab.com/slusheea/stk8ba58/)
Driver crate for the STK8BA58 three axis MEMS accelerometer 

### <img src="https://gitlab.com/uploads/-/system/project/avatar/52187063/repopic.png" width="24"/> [gy-21](https://gitlab.com/slusheea/gy-21)
Driver crate for the HTU21D temperature and relative humidity sensor

## Web 
### <img src="https://gitlab.com/uploads/-/system/project/avatar/67154834/repopic.png" width="24"/> [Statusbox](https://gitlab.com/slusheea/statusbox)
A way to express my fleeting thouhghts on my website

### <img src="https://gitlab.com/uploads/-/system/project/avatar/66349211/repopic.png" width="24"/> [webrooming](https://gitlab.com/slusheea/webrooming)
A 3D interactive version of my room on the web

### <img src="https://gitlab.com/uploads/-/system/project/avatar/64467836/repopic.png" width="24"/> [About me](https://gitlab.com/slusheea/about_me)
The repository for my personal website

## Misc
### <img src="https://gitlab.com/uploads/-/system/project/avatar/36516802/rustyberry.jpg" width="24"/> [RP2040 Rust template](https://gitlab.com/slusheea/rp2040-template)
Generate a new project for an RP2040 developement board with your preferred frameworks pre-configured

### <img src="https://gitlab.com/uploads/-/system/project/avatar/55051734/repopic.png" width="24"/> [Dotfiles](https://gitlab.com/slusheea/dotfiles)
My personal dotfiles